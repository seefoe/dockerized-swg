#!/bin/bash
basedir=$PWD

if [[ -z "${SWG_BUILD_DSRC}" ]]; then
	echo "Running Server"
	cd $basedir/cfg/
	$basedir/bin/LoginServer -- @servercommon.cfg &
	$basedir/bin/TaskManager -- @servercommon.cfg 
else
	echo "Building DSRC"
	$basedir/utils/content/build_java_multi.sh
	$basedir/utils/content/build_miff.sh
	$basedir/utils/content/build_tab.sh
	$basedir/utils/content/build_tpf_multi.sh
	$basedir/utils/content/build_object_template_crc_string_tables.py
	$basedir/utils/content/build_quest_crc_string_tables.py
	echo "Done building DSRC"
fi
