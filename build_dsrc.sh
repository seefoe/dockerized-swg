#!/bin/bash
basedir=$PWD

# ensure that repo has been setup
$basedir/utils/initial_setup.sh

# run the docker image and build the source
docker run -v $basedir/gameserver/:/swg/:z -v $basedir/dsrc/:/swg/dsrc/:z -e SWG_BUILD_DSRC=TRUE swg-runtime

# cleanup any artifacts
rm -rf $basedir/gameserver/dsrc
