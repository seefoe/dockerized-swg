#!/bin/bash
basedir=$PWD

# ensure that repo has been setup
$basedir/utils/initial_setup.sh

# run the docker image and build the source
docker run -v $basedir/src/:/swg/:z swg-compiler

# copy bins to our gameserver folder
cp $basedir/src/build/bin/* $basedir/gameserver/bin/
