#!/bin/bash
basedir=$PWD

# grab latest version
cd $basedir/src/ && git pull
cd $basedir/dsrc/ && git pull
cd $basedir/ && git pull

# build src and dsrc
cd $basedir
$basedir/build_src.sh
$basedir/build_dsrc.sh
