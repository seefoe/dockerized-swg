if [ ! -d "utils/redist" ]; then
	mkdir utils/redist
fi

if [ ! -f ./utils/redist/oracle-instantclient12.1-basic-12.1.0.2.0-1.i386.rpm ]; then
	wget https://bitbucket.org/seefoe/src/downloads/oracle-instantclient12.1-basic-12.1.0.2.0-1.i386.rpm -P ./utils/redist/
fi

if [ ! -f ./utils/redist/oracle-instantclient12.1-devel-12.1.0.2.0-1.i386.rpm ]; then
	wget https://bitbucket.org/seefoe/src/downloads/oracle-instantclient12.1-devel-12.1.0.2.0-1.i386.rpm -P ./utils/redist/
fi

if [ ! -f ./utils/redist/oracle-instantclient12.1-sqlplus-12.1.0.2.0-1.i386.rpm ]; then
	wget https://bitbucket.org/seefoe/src/downloads/oracle-instantclient12.1-sqlplus-12.1.0.2.0-1.i386.rpm -P ./utils/redist/
fi

if [ ! -f ./utils/redist/IBMJava2-SDK-1.4.2-13.18.tgz ]; then
	wget https://bitbucket.org/seefoe/src/downloads/IBMJava2-SDK-1.4.2-13.18.tgz -P ./utils/redist/
fi

if [ ! -f ./utils/redist/jdk-8u151-linux-i586.tar.gz ]; then
	wget https://bitbucket.org/seefoe/src/downloads/jdk-8u151-linux-i586.tar.gz -P ./utils/redist/
fi
