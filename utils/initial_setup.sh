#!/bin/bash
basedir=$PWD

if [ ! -f $basedir/.setup ]; then
	# clone repos
	$basedir/utils/clone_repos.sh

	# prepare swg-compiler
	$basedir/utils/get_redist.sh
	$basedir/utils/build_docker_images.sh

	# create indicator of complete setup
	touch $basedir/.setup
fi
