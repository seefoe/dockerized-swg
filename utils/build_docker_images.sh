# build the swg-compiler docker image from our dockerfile
docker build -t swg-compiler -f src/Dockerfile .

# build the swg-runtime docker image from our dockerfile
docker build -t swg-runtime -f gameserver/Dockerfile .
