#!/bin/bash
basedir=$PWD

# ensure that repo has been setup
$basedir/utils/initial_setup.sh

# run the docker image and build the source
docker kill swg
docker rm swg
docker run -p 44453:44453/udp -p 44462-44463:44462-44463/udp -h swg -v $basedir/gameserver/:/swg/:z -v $basedir/dsrc/:/swg/dsrc/:z --name swg swg-runtime
