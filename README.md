Star Wars: Galaxies
===================

This repository contains a bootstrapper for compiling the Star Wars: Galaxies game server.

----------

##### Prequisities
* Linux
* Docker

##### Building latest version
* As root, `./build_latest.sh`

##### Building game server binaries
* As root, `./build_src.sh`

##### Building game server content
* As root, `./build_dsrc.sh`

##### Running gameserver
* Run `./build_latest.sh` to ensure you have built both the server binaries and game data
* Copy the configs inside `gameserver/cfg/example` to `gameserver/cfg`
* Modify the configs to reflect the settings you would like
* Update Oracle DB username and password
* As root, `./run_server.sh`

##### Additional notes
After building either parts of the game server, you may find their result inside the gameserver folder.
